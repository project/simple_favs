/**
 * @file
 * Defines the simpleFavs Behaviour.
 */

(function ($, Drupal) {
  Drupal.behaviors.simpleFavs = {
    /**
     * Adds to the favourites storage the given contentId.
     *
     * @param contentId
     *   The content id to be stored as favourite.
     */
    addToFavs: function (contentId) {
      var favs = Drupal.behaviors.simpleFavs.getFavs();

      // Add the new content to favs.
      favs[contentId] = contentId;
      Drupal.behaviors.simpleFavs.setCookie(favs);
    },

    /**
     * Removed from the favourites storage the given contentId.
     *
     * @param contentId
     *   The content id to be removed as favourite.
     */
    removeFromFavs: function (contentId) {
      var favs = Drupal.behaviors.simpleFavs.getFavs();
      delete favs[contentId];
      Drupal.behaviors.simpleFavs.setCookie(favs);
    },

    /**
     * Sets the favs cookie.
     */
    setCookie: function (cookieValue) {
      const cookieExpires = Drupal.behaviors.simpleFavs.getExpirationDate();
      document.cookie = 'content_favs=' + JSON.stringify(cookieValue) + ';expires=' + cookieExpires.toGMTString() + ';path=/';
    },

    /**
     * Defines the cookie expiration time based on the configuration.
     *
     * @returns {Date}
     *   The expiration date for the cookie.
     */
    getExpirationDate: function () {
      var now = new Date();
      const cookieLifetime = parseInt(drupalSettings.simple_favs.cookie_expiration);
      // We set the expiration date in cookie_expiration days.
      now.setDate(now.getDate() + cookieLifetime);

      return now;
    },

    /**
     * Retrieves the list of stored favourites.
     */
    getFavs: function () {
      const name = "content_favs=";
      const decodedCookie = decodeURIComponent(document.cookie);
      const ca = decodedCookie.split(';');

      for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) === ' ') {
          c = c.substring(1);
        }
        if (c.indexOf(name) === 0) {
          return JSON.parse(c.substring(name.length, c.length));
        }
      }
      return {};
    },

    /**
     * Checks if the given contentId is in favourites or not.
     *
     * @param contentId
     *   The content id to be checked.
     *
     * @return boolean
     *   TRUE if the given contentId is stored as favourite content. FALSE
     *   otherwise.
     */
    isInFavs: function (contentId) {
      const favs = Drupal.behaviors.simpleFavs.getFavs();

      return favs.hasOwnProperty(contentId);
    },

  };
})(jQuery, Drupal);
